﻿using NETProjectTutorial.implements;
using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace NETProjectTutorial.model
{
    class ProductoModel
    {
        private static  List<Producto> ListProductos = new List<Producto>();
        private implements.DaoImplementsProducto daoProducto;

        public ProductoModel()
        {
            daoProducto = new implements.DaoImplementsProducto();
        }

        public List<Producto> GetListProducto()
        {
            return daoProducto.findAll();
        }

        public void Populate()
        {
            ListProductos = JsonConvert.DeserializeObject<List<Producto>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Producto_data));

            foreach (Producto p in ListProductos)
            {
                daoProducto.save(p);
            }
        }

        public void save(DataRow producto)
        {
            Producto p = new Producto();
            p.Id = Convert.ToInt32(producto["Id"].ToString());
            p.Sku = producto["SKU"].ToString();
            p.Nombre = producto["Nombre"].ToString();
            p.Descripcion = producto["Descripción"].ToString();
            p.Cantidad = Convert.ToInt32(producto["Cantidad"].ToString());
            p.Precio = Convert.ToDouble(producto["Precio"].ToString());

            daoProducto.save(p);
        }

        public void update(DataRow producto)
        {
            Producto p = new Producto();
            p.Id = Convert.ToInt32(producto["Id"].ToString());
            p.Sku = producto["SKU"].ToString();
            p.Nombre = producto["Nombre"].ToString();
            p.Descripcion = producto["Descripción"].ToString();
            p.Cantidad = Convert.ToInt32(producto["Cantidad"].ToString());
            p.Precio = Convert.ToDouble(producto["Precio"].ToString());

            daoProducto.update(p);
        }

        public void delete(DataRow producto)
        {
            Producto p = new Producto();
            p.Id = Convert.ToInt32(producto["Id"].ToString());
            p.Sku = producto["SKU"].ToString();
            p.Nombre = producto["Nombre"].ToString();
            p.Descripcion = producto["Descripción"].ToString();
            p.Cantidad = Convert.ToInt32(producto["Cantidad"].ToString());
            p.Precio = Convert.ToDouble(producto["Precio"].ToString());

            daoProducto.delete(p);
        }
    }
}
