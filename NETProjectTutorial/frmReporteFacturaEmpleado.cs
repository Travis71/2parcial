﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmReporteFacturaEmpleado : Form
    {
        private DataSet dsProductos;
        private BindingSource bsProductos;

        public DataSet DsProductos
        {
            get
            {
                return dsProductos;
            }

            set
            {
                dsProductos = value;
            }
        }

        public BindingSource BsProductos
        {
            get
            {
                return bsProductos;
            }

            set
            {
                bsProductos = value;
            }
        }

        public FrmReporteFacturaEmpleado()
        {
            InitializeComponent();
            bsProductos = new BindingSource();
        }

        private void FrmReporteFacturaEmpleado_Load(object sender, EventArgs e)
        {
            bsProductos.DataSource = DsProductos;
            bsProductos.DataMember = DsProductos.Tables["ReporteFacturaEmpleado"].TableName;
            dgvProductos.DataSource = bsProductos;
            dgvProductos.AutoGenerateColumns = true;

            DataTable temp1 = dsProductos.Tables["ReporteFactura"];
            DataTable temp2 = dsProductos.Tables["Empleado"];

            
        }
    }
}
