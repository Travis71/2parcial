﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrmNotePad.Secuencial
{
    class SecuencialStream
    {
        private string filepath;

        public SecuencialStream(string filename)
        {
            this.filepath = filename;
        }

        public void WriteText(string text)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(filepath))
                {
                    sw.Write(text);
                }
            }
            catch (IOException) { }
        }

        public string ReadText()
        {
            string text = "";
            try
            {
                using (StreamReader sr = new StreamReader(filepath))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null) { }
                    {
                        text += line;
                    }
                }
            }
            catch (IOException) { }

            return text;
        }
    }
}
